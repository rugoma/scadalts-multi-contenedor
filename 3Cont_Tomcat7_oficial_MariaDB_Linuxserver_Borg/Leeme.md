# Pila (stack) de contenedores para ScadaBR/ScadaLTS

R. Gómez Antolí. 2019
GPL v3

## Imagen de contenedor

La imagen a construir para ScadaBR/ScadaLTS está basada en la oficial de 
Tomcat7 proporcionada por Docker, a la cual se le ha añadido el supervisor S6,
se puede construir a partir de aquí:

https://gitlab.com/rugoma/tomcat-docker-debian/tree/master/Debian_Tomcat7_Oficial

## Contenedor de la BBDD

Directamente de la imagen proporcionada por Linuxserver.io sin modificar nada a
excepción de los parámetros de Mysql necesarios para ScadaBR/ScadaLTS

## Contenedor de copias de seguridad

Basado en [Borg-Mysql-backup de Laur Aliste](
https://github.com/laur89/docker-borg-mysql-backup) con la única modificación
de eliminar el usuario y la contraseña en los parámetros para añadir un 
fichero de credenciales.

Esta modificación está por publicar, puedes utilizar la imagen de Laur
directamente modificando el *docker-compose.yml*.

## Utilización

La forma más fácil de utilizar esta combinación de contenedores es a través
del *docker-compose.yml* incluido en este repositorio.

Simplemente ajuste la ruta de los volúmenes hacia los datos en todos los
contenedores: Tomcat, MariaDB y Borg.

- El contenedor de Tomcat debe apuntar hacia el directorio que contiene a 
  ScadaBR
- El contenedor de MariaDB debe apuntar hacia el directorio que albergue la
  base de datos
- El contenedor de Borg debe apuntar hacia el directorio que albergue las
  copias de seguridad.

### Utilerías

1. Lista_copiasseg.sh: Lista las copias de seguridad realizadas
1. Restaura.sh: Restaura una copia de seguridad concreta

Uso de *Restaura.sh*:

```{bash}
./Restaura.sh nombre-copia-seguridad
```
Tanto el guión *Lista_copiasseg.sh* como *Restaura.sh* suponen que las copias
de seguridad se encuentran en la ruta predefinida en el fichero 
*docker-compose.yml*, en caso de cambiar deben ajustarse o crear enlaces 
simbólicos.

