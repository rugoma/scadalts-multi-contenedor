# Pila (stack) de contenedores para ScadaBR/ScadaLTS

R. Gómez Antolí. 2019
GPL v3

## Imagen de contenedor

La imagen a construir para ScadaBR/ScadaLTS está basada en la oficial de 
Tomcat7 proporcionada por Docker, a la cual se le ha añadido el supervisor S6,
se puede construir a partir de aquí:

https://gitlab.com/rugoma/tomcat-docker-debian/tree/master/Debian_Tomcat7_Oficial

## Contenedor de la BBDD

Directamente de la imagen proporcionada por Linuxserver.io sin modificar nada a
excepción de los parámetros de Mysql necesarios para ScadaBR/ScadaLTS

## Contenedor de copias de seguridad

Basado en [Borg-Mysql-backup de Laur Aliste](
https://github.com/laur89/docker-borg-mysql-backup) con la única modificación
de eliminar el usuario y la contraseña en los parámetros para añadir un 
fichero de credenciales.

Esta modificación está por publicar, puedes utilizar la imagen de Laur
directamente modificando el *docker-compose.yml*.

## Utilerías

1. Lista_copiasseg.sh: Lista las copias de seguridad realizadas
1. Restaura.sh: Restaura una copia de seguridad concreta

Uso de *Restaura.sh*:

```{bash}
./Restaura.sh nombre-copia-seguridad
```

