# Multicontenedor para ScadaBR/LTS con Mariadb y copias de seguridad

Con base en la imagen creada para ScadaBR se proporciona un docker-compose.yml
preparado para lanzar tres contenedores:

1. Tomcat preparado para ScadaBR/LTS
1. Servidor MariaDB
1. Contenedor con Borg y Mysqldump como herramientas de copias de seguridad

# Utilerías

Para no tener que lanzar comandos a mano para restaurar se escriben dos 
pequeñas utilerías:

1. Lista_copiasseg.sh: Lista las copias de seguridad realizadas
1. Restaura.sh: Restaura una copia de seguridad concreta

Uso de *Restaura.sh*:

```{bash}
./Restaura.sh nombre-copia-seguridad
```

# Por hacer

Mejorar las utilerías para crear una herramienta unificada que, al lanzarla,
te saque un listado de las copias de seguridad y te permita elegir cual 
recuperar.

Para mejorar la interfaz se podría utilizar:

- [Bash Infinity](https://github.com/niieani/bash-oo-framework)
- [Bullet para Python](https://github.com/Mckinsey666/bullet)
- Tput. [1](https://linuxhint.com/tput-printf-and-shell-expansions-how-to-create-awesome-outputs-with-bash-scripts/)
     y [2](http://linuxcommand.org/lc3_adv_tput.php)

# Refencias

- Contenedores utilizados:
    * [MariaDB de Linuxserver](https://github.com/linuxserver/docker-mariadb)
    * [Borg-Mysql-backup de Laur Aliste](
	https://github.com/laur89/docker-borg-mysql-backup)
