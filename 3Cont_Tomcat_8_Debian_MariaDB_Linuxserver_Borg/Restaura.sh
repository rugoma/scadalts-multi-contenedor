#!/bin/sh

USU=$(whoami)

echo "Creamos un directorio temporal con las copias de seguridad."
echo "Introduzca la contraseña de superusuario:"
su -c "cp -r ./copias_seguridad/ ./tempo"

docker run -it --rm \
    -e BORG_PASSPHRASE=bioscadacontra \
    -v ${PWD}/tempo:/backup \
       local/borg-mysql-copseg -- restore.sh -l -a $1

echo "Se cambian los permisos de acceso del directorio temporal"
echo "Introduzca la contraseña de superusuario:"
su -c "chown -R ${USU}.${USU} ./tempo" 

## Hay una forma de automatizar todo el proceso de restauración
## pero, por ahora, no me interesa.
#   -e MYSQL_HOST=scada_todo_en_uno \
#   -e MYSQL_PORT=3306 \
#   -e MYSQL_USER=scadabr \
#   -e MYSQL_PASS=scadabr \
#   -e BORG_PASSPHRASE=bioscadacontra \
#   -v ${PWD}/Restaura:/backup \
#   -v ${PWD}/configuracion:/config:ro \
#       local/borg-mysql-copseg -- restore.sh -l -d -a $1

